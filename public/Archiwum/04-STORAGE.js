import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import { 
    getStorage,
    ref,
    uploadBytesResumable,
    getDownloadURL,
    listAll
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyDHI9vz9TXlW9Oovmrv0Z7bGHv_xXZYv9w",
    authDomain: "zdfronpol11.firebaseapp.com",
    databaseURL: "https://zdfronpol11-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "zdfronpol11",
    storageBucket: "zdfronpol11.appspot.com",
    messagingSenderId: "680071626825",
    appId: "1:680071626825:web:cba7b4f8d84a73d31b7f98"
};

// Inicjalizowanie modułów
const app = initializeApp(firebaseConfig);
const storage = getStorage();

// Mapowanie elementów z UI
const imageInput = document.querySelector('#imageInput');
const imageName = document.querySelector('#imageName');
const uploadButton = document.querySelector('#uploadButton');
const uploadImageProgressBar = document.querySelector('#uploadImageProgressBar');
const uploadedImage = document.querySelector('#uploadedImage');

// ----- WRZUCANIE PLIKU Z DYSKU LOKALNEGO NA STORAGE -----
// 1. Zmienne pomocnicze do przechowania pliku
let fileToUpload;
let fileExtension;

// 2. Proces wyboru pliku
// Wbijamy się w moment, kiedy plik zostanie wybrany, więc event 'change' zostanie wywołany
imageInput.onchange = (event) => {
    // Zapamiętanie całego pliku, który wrzucimy na Storage
    fileToUpload = event.target.files[0];

    // Pobranie pełnej nazwy wybranego przez użytkownika pliku z eventu
    const fullFileName = event.target.files[0].name;
    
    // Podział nazwy wybranego pliku na część z oryginalną nazwą oraz rozszerzeniem pliku
    const splittedFileName = fullFileName.split('.');
    
    // Zawsze wybieramy ostatni element z tablicy, bo tam zawsze będzie rozszerzenie pliku
    // Także jesteśmy bezpieczni nawet w przypadku nazw typu a.b.c.jpg
    fileExtension = splittedFileName[splittedFileName.length - 1];
};

// 3. Przesyłanie wybranego pliku do Firebase (Storage)
// Wbijamy się w moment kliknięcia w przycisk
uploadButton.onclick = async (event) => {
    // Pobranie nazwy docelowego pliku z input
    const fileName = imageName.value;

    // Złączenie docelowej nazwy pliku z roszerzeniem pliku
    const fullFileName = fileName + '.' + fileExtension;
    
    // Określenie metadanych
    const metaData = {
        contentType: fileToUpload.type
    };

    // 4. Komunikacja ze Storage w celu wrzucenia pliku
    const uploadProcess = uploadBytesResumable(ref(storage, 'Images/' + fullFileName), fileToUpload, metaData);

    // 5. Wbijamy się w moment, kiedy status wrzucania pliku będzie zmieniony
    uploadProcess.on('state_changed', 
    // CO ROBIĆ W TRAKCIE?
    (snapshot) => {
        const progress = snapshot.bytesTransferred / snapshot.totalBytes * 100;
        uploadImageProgressBar.innerHTML = Math.round(progress) + '%';
    }, 
    // CO ROBIĆ KIEDY BŁĄD?
    (error) => {
        console.log(error);
    }, 
    // CO ROBIĆ KIEDY KONIEC Z SUKCESEM?
    async () => {
        const imageUrl = await getDownloadURL(uploadProcess.snapshot.ref);
        uploadedImage.setAttribute('src', imageUrl);
    });
};

// 6. Pobranie wszystkich dostępnych obrazków (w folderze)
const allImages = await listAll(ref(storage, 'Images'));
allImages.items.forEach(async (image) => {
    // console.log(image._location.path);
    const imageUrl = await getDownloadURL(image);
    console.log(imageUrl);
});

// Wybierak: https://www.w3schools.com/tags/tag_select.asp
// Jak wygenerować odpowiedni HTML? -> Spójrz Archiwum/01-CRUD.js

// 7. Pobranie konkretnego obrazka
const imageUrl = await getDownloadURL(ref(storage, 'Images/kot2.jpg'));
uploadedImage.setAttribute('src', imageUrl);

/*
    ------------- SECURITY RULES -------------
    1. Zablokowanie możliwości wrzucania plików osobom niezalogowanym

    rules_version = '2';
    service firebase.storage {
        match /b/{bucket}/o {
            match /{allPaths=**} {
                allow read: if true;
                allow write: if request.auth != null;
            }
        }
    }

    2. Zablokowanie możliwości wrzucania obrazków:
        - większych niż 5MB
        - innych niż obrazki 
        - tylko z rozszerzeniem .jpg

    rules_version = '2';
    service firebase.storage {
    match /b/{bucket}/o {
        match /{allPaths=**} {
            allow read: if true;
            allow write: if request.resource.size < 5 * 1024 * 1024
                                && request.resource.contentType.matches('image/jpeg')
                                && request.resource.name.matches('.*jpg');
            }
        }
    }

*/

// AUDIO: https://www.w3schools.com/html/html5_audio.asp